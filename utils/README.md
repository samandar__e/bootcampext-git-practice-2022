# Pipenv
## Instal pipenv

```bash
pip3 install --user pipenv==2021.5.29
```

## Activate

```bash
pipenv shell
```

## Install a package

```bash
pipenv install python-gitlab==3.1.1
```

## Set lockfile - before deployment

```bash
pipenv lock
```

## Exiting the virtualenv

```bash
exit
```

# gitignore

[Collection of gitignore](https://github.com/github/gitignore)
