import os
import gitlab

URL = 'https://gitlab.com'
GITLAB_PROJECT_ID = 34947841
GITLAB_PRIVATE_TOKEN = os.environ.get('GITLAB_PRIVATE_TOKEN', None)
DEBUG = True
MAX_ISSUE_COUNT = 50

if not GITLAB_PRIVATE_TOKEN:
    print('Missing gitlab private token, please provide GITLAB_PRIVATE_TOKEN')
    exit()

# private token or personal token authentication (GitLab.com)
gl = gitlab.Gitlab(URL, GITLAB_PRIVATE_TOKEN)

# Get a project by ID
project = gl.projects.get(GITLAB_PROJECT_ID)


for i in range(MAX_ISSUE_COUNT):
    if not DEBUG:
        # Create issue
        issue = project.issues.create({'title': f'''Добавить в приложение route /red{i:02d}''',
                                      'description': f'''Добавить в приложение route `/red{i:02d}`, ''' +
                                       f'''возращающий {i:02d}.'''})
    else:
        print({'title': f'''Добавить в приложение route /red{i:02d}''',
              'description': f'''Добавить в приложение route `/red{i:02d}`, возращающий {i:02d}.'''})
